name := "play-ws-test"

version := "1.0-SNAPSHOT"

classpathTypes += "maven-plugin"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache
)     

// https://mvnrepository.com/artifact/com.google.code.gson/gson
libraryDependencies += "com.google.code.gson" % "gson" % "1.7.1"

libraryDependencies += "org.apache.logging.log4j" % "log4j-api" % "2.10.0"

libraryDependencies += "org.apache.logging.log4j" % "log4j-core" % "2.10.0"

play.Project.playJavaSettings
