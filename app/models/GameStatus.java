package models;

public enum GameStatus {
    START_GAME,
    START_GAME_ERROR,
    GAME_STARTED,
    NEXT_MOVE,
    MOVE_ERROR,
    GAME_OVER,
    GAME_STOPPED,
    MATRIX_ERROR,
    UNKNOWN_STATUS,
    PARSE_JSON_ERROR,
    PARSE_STRING_ERROR,
    STOP_GAME,
    STOP_GAME_ERROR
}
