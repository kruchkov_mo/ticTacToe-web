#TicTacToe online

Tic-tac-toe (also known as noughts and crosses) is a paper-and-pencil game for two players, X and O, who take turns marking the spaces in a infinity field. The player who succeeds in placing five of their marks in a horizontal, vertical, or diagonal row wins the game.


### Online version

You can try this game online [here](https://chat-0108711.herokuapp.com/ "here")

![](https://lh3.googleusercontent.com/g182NXFjSDoLv5XgvGiAnbXxBgcnWkDVHcLv_yb66NlbKj3fputLLjlurB-4MUshS973T7CavkFJsC6qKl5ReqoHet_gJwBeUOjsRvEebYCvwqmFQfR1mCLTBIPJ-KT-ZR5D0fLPxiZWfOo1IWtqIfE1vrBUPa4b9lm161qIysCOwiUk5JeCx3OwAUAXdhby9fq1vuw6dKt_vhDjZFbOLUbRR_qXLu5NmC2Z7ZllFFBR4-pAXJXG1_wpveQUpm6UgKpuNn2uD0k9h-BhiLeZ92VvNeV_EPHL-Y5kFEAe49-t3UPNx2zxNyWxbXpBzlYP1RvBsaQL_oyDlLv_zToN0YW0RKKfe1r6nNkAeGqohEnjunXa-LTNIvap6F0MJ_-9WZb1hDudh0Ksj6CIpGzCH4tnMvB4VZ9ZW0K7lLNfog6q6wzwAcDOvIvYAQlVLsfVkHrgDm-RDIP4qlx05yszlm3KJgZ9UZJFpwHJKFzAlC5UJvzV-pdpmV9l2SdAhLIIqPsq-QebbxvKagE9132Ms1XttfSUF9D-YD1Xii2hAx18UFUPF54v9_0uxVXV-KOw_-pzq8eWD5kA35G5kbHUlfpJAHNLL_C8QET8lgfWjscDpBv8N9dCVTQA-JkBBkfef1ERnCvjpgt2XXQSYXfJAgvrN1dwbnet=w908-h476-no)
